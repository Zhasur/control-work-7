import React, { Component } from 'react';
import './App.css';
import Menu from '../../Components/Menu/Menu'
import hamburger from '../../Assets/hamburger.png'
import cheeseburger from '../../Assets/cheeseburger.png'
import fries from '../../Assets/fries.png'
import coffee from '../../Assets/coffee.png'
import tea from '../../Assets/tea.png'
import cola from '../../Assets/cola.png'
import OrderLists from "../../Components/Orders/OrderLists";


class App extends Component {
    state = {
        menu: [
            {name: 'Hamburger', count: 0, price: 80,image: hamburger, show: false},
            {name: 'Cheeseburger', count: 0, price: 90,image: cheeseburger, show: false},
            {name: 'Fries', count: 0, price: 45,image: fries, show: false},
            {name: 'Coffee', count: 0, price: 70,image: coffee, show: false},
            {name: 'Tea', count: 0, price: 50,image: tea, show: false},
            {name: 'Cola', count: 0, price: 40,image: cola, show: false},
        ],
        emptyOrder: {text: 'Order is empty!'},
        PleaseAdd: {text: 'Please add some items!'},
        total: 0,
        showTotal: false,
    };


    addCount = (name) => {

        const item = [...this.state.menu];

        let countedTotal = this.state.total;
        let showTotal = this.state.showTotal;

        for(let i = 0; i < item.length; i++) {
            if(item[i].name === name) {
                item[i].count++;

                countedTotal += item[i].price;

                item[i].show = true;

                showTotal = true;

            }
        }

        this.setState({menu: item, total: countedTotal, showTotal: showTotal});
    };

    subtractCount = (name) => {
        const subtract = [...this.state.menu];
        let countedTotal = this.state.total;
        let showTotal = this.state.showTotal;

        for(let i = 0; i < subtract.length; i++) {
            if(subtract[i].name === name) {
                if (subtract[i].count !== 0) {
                    subtract[i].count--;
                    countedTotal -= subtract[i].price;
                    if (countedTotal === 0) {
                        showTotal = false;
                    }
                    if (subtract[i].count < 1){
                        subtract[i].show = false
                    }
                }
            }
        }
        this.setState({menu: subtract, total: countedTotal, showTotal: showTotal})
    };


    render() {
        const showTotal = this.state.showTotal;
        return (
              <div className="App Container">
                  <div className="add-items">
                      <h2 className='block-title'>Add items</h2>
                      <Menu
                          menu={this.state.menu}
                          onClickAdd={(name) => this.addCount(name)}
                      />
                  </div>
                  <div className="order-block">
                      <h2 className='block-title'>Order Details</h2>
                      {showTotal ? null :
                          <p className="empty">
                              {this.state.emptyOrder.text}
                              <span>{this.state.PleaseAdd.text}</span>
                          </p>

                      }

                      <OrderLists
                          menuLists={this.state.menu}
                          total={this.state.total}
                          remove={this.subtractCount}
                          showTotal={showTotal}
                      />

                  </div>
              </div>
        );
    }
}

export default App;
