import React from 'react'
import Order from "./OrderList"

const OrderLists = (props) => {
    const showTotal = props.showTotal;
    return (
        <div className="order-list">

            {props.menuLists.map((item, key) => {
                const show = item.show;
                return (
                    <div key={key}>
                        <Order
                            name={item.name}
                            count={item.count}
                            price={item.price}
                            show={show}
                            onClick={() => props.remove(item.name)}
                            emptyOrder={props.emptyOrder}
                        />
                    </div>

                )
            })}
            {showTotal ?
                <div className="total-price-block">
                    <span className="total-price">Total price: {props.total}</span>
                </div> : null}

        </div>
    )
};

export default OrderLists