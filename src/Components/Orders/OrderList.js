import React from 'react'

const Order = (props) => {
    const show = props.show;

    const hidden = {
        display: show ? "block" : "none"
    };

        return (
            <div className="order" key={props.key} style={hidden}>
                <h5 className="ordered-item">
                    {props.name}
                </h5>

                <span className="amount">
                    x {props.count}
                </span>

                <i className="price">
                    {props.price} KGS
                </i>

                {show ? <button
                    className="subtract-btn"
                    onClick={props.onClick}
                    style={{display: 'inline-block'}}
                > Delete
                </button> : null}
            </div>
        );

};

export default Order