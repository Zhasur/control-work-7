import React from 'react';
import './Menu.css'

const Menu = (props) => {

    return (
        props.menu.map((fill, key) => {

            return (
                <div className="item" key={key} onClick={() => props.onClickAdd(fill.name)}>

                    <img src={fill.image} alt="#" className="img" />

                    <div className="inner-item">
                        <h4 className="item-name">{fill.name}</h4>
                        <span className="item-price">Price: {fill.price} KGS</span>
                    </div>
                </div>
            )

        })

    )
};

export default Menu;